/**
 * Licensed Materials - Property of ReqPro.com / SmarterProcess © Copyright ReqPro 2020
 * Contact information: info@reqpro.com
 */

var moduleRef;

var parameterName;

var runningProcess = false;

$(function () {
  if (window.RM) {
    var prefs = new gadgets.Prefs();
    parameterName = prefs.getString("parameterName");

    $("#selectTermsButton").on("click", function () {
      selectGlossaryTerms();
    });

    $("#findAllTerms").on("click", function () {
      findAllGlossaryTerms();
    });

    gadgets.window.adjustHeight();

    //
  } else {
    clearSelectedInfo();
    lockAllButtons();
    $(".status")
      .addClass("incorrect")
      .html("<b>Error:</b> The widget can be used only inside IBM Engineering DOORS Next.");
    gadgets.window.adjustHeight();
  }
});

// Subscribe to the Artifact Selection event
RM.Event.subscribe(RM.Event.ARTIFACT_SELECTED, async function (selected) {
  var outcome = "";
  if (selected.length === 1) {
    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");

        process(selected);
      });
    $(".status").removeClass("warning correct incorrect");
    $(".status").html("<b>Message:</b> One artifact is selected.");
  } else if (selected.length > 1) {
    unlockButton("#actionButton");
    $("#actionButton").unbind();
    $(".status").removeClass("warning correct incorrect");
    $("#actionButton")
      .css("display", "inline")
      .on("click", async function () {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");
        process(selected);
      });
    $(".status").html(`<b>Message:</b> ${selected.length} artifacts are selected.`);
  } else {
    // clear the display area...
    lockButton("#actionButton");
    $("#actionButton").unbind();
    clearSelectedInfo();
    // inform the user that they need to select only one thing.
    $(".status").html("<b>Message:</b> Select one or multiple objects.");
    gadgets.window.adjustHeight();
  }
});

RM.Event.subscribe(RM.Event.ARTIFACT_OPENED, function (ref) {
  RM.Data.getAttributes(ref, RM.Data.Attributes.FORMAT, checkIfModule);
});

RM.Event.subscribe(RM.Event.ARTIFACT_CLOSED, function (ref) {
  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
  $("#actionInModuleButton").off("click");
  moduleRef = null;
});

async function process(selected) {
  var outcome = "";
  var respose;

  return new Promise(async function (resolve, reject) {
    for (var q = 0; q < selected.length; q++) {
      var obj = selected[q]; // tutaj

      var opResult = await getAttributes(obj); // tutaj

      if (opResult.code !== RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
      } else {
        lockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status")
          .addClass("warning")
          .html(`<b>Message:</b> Processing artifact ${q + 1} out of ${selected.length}.`);

        var toSave = [];

        for (artAttrs of opResult.data) {
          var tmp = artAttrs.values[RM.Data.Attributes.PRIMARY_TEXT];
          var type = artAttrs.values[RM.Data.Attributes.ARTIFACT_TYPE].name;

          if (type.indexOf("Threat Scenario") > -1) {
            var preSeverity = artAttrs.values["Pre-Mitigation Severity"];
            var preExploit = artAttrs.values["Pre-Mitigation Ability to Exploit"];
            var preRating = artAttrs.values["Pre-Mitigation Secure Risk Rating"];

            var postSeverity = artAttrs.values["Post-Mitigation Severity"];
            var postExploit = artAttrs.values["Post-Mitigation Ability to Exploit"];
            var postRating = artAttrs.values["Pre-Mitigation Secure Risk Rating"];

            console.log("Hit");
            console.log(preSeverity);
            console.log("Hit2");
            console.log(preExploit);

            if (preExploit == null || preSeverity == null) {
              artAttrs.values["Pre-Mitigation Secure Risk Rating"] = "";
            } else {
              if (preExploit.indexOf("1") > -1 || preExploit.indexOf("2") > -1) {
                artAttrs.values["Pre-Mitigation Secure Risk Rating"] = "Acceptable";
              } else if (preExploit.indexOf("3") > -1) {
                if (preSeverity.indexOf("4") > -1) {
                  artAttrs.values["Pre-Mitigation Secure Risk Rating"] = "Unacceptable";
                } else {
                  artAttrs.values["Pre-Mitigation Secure Risk Rating"] = "Acceptable";
                }
              } else if (preExploit.indexOf("4") > -1) {
                if (preSeverity.indexOf("4") > -1 || preSeverity.indexOf("3") > -1) {
                  artAttrs.values["Pre-Mitigation Secure Risk Rating"] = "Unacceptable";
                } else {
                  artAttrs.values["Pre-Mitigation Secure Risk Rating"] = "Acceptable";
                }
              }
            }

            if (postExploit == null || postSeverity == null) {
              artAttrs.values["Post-Mitigation Secure Risk Rating"] = "";
            } else {
              if (postExploit.indexOf("1") > -1 || postExploit.indexOf("2") > -1) {
                artAttrs.values["Post-Mitigation Secure Risk Rating"] = "Acceptable";
              } else if (postExploit.indexOf("3") > -1) {
                if (postSeverity.indexOf("4") > -1) {
                  artAttrs.values["Post-Mitigation Secure Risk Rating"] = "Unacceptable";
                } else {
                  artAttrs.values["Post-Mitigation Secure Risk Rating"] = "Acceptable";
                }
              } else if (postExploit.indexOf("4") > -1) {
                if (postSeverity.indexOf("4") > -1 || postSeverity.indexOf("3") > -1) {
                  artAttrs.values["Post-Mitigation Secure Risk Rating"] = "Unacceptable";
                } else {
                  artAttrs.values["Post-Mitigation Secure Risk Rating"] = "Acceptable";
                }
              }
            }

            toSave.push(artAttrs);
          }
        }
        if (toSave.length != 0) {
          var z = await modifyArtifacts(toSave);
          respose = z.code;
        } else {
          respose = RM.OperationResult.OPERATION_OK;
        }
      }

      if (respose != RM.OperationResult.OPERATION_OK) {
        unlockAllButtons();
        $(".status").removeClass("warning correct incorrect");
        $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
        return;
      }
    }
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("correct").html("<b>Success:</b> All selected artifacts(s) were processed.");
    resolve("");
  });
}

function checkIfModule(opResult) {
  if (opResult.code === RM.OperationResult.OPERATION_OK) {
    var attrs = opResult.data[0];

    if (attrs.values[RM.Data.Attributes.FORMAT] === RM.Data.Formats.MODULE) {
      moduleRef = attrs.ref;
      unlockButton("#actionInModuleButton");
      $("#actionInModuleButton").on("click", processForModule);
    } else {
      lockButton("#actionInModuleButton");
    }
  }
}

async function processForModule() {
  lockAllButtons();

  $(".status").removeClass("warning correct incorrect");
  $(".status").addClass("warning").html("<b>Message:</b> Referencing terms in progress.");

  var result = await getModuleArtifacts(moduleRef);

  if (result.code !== RM.OperationResult.OPERATION_OK) {
    unlockButton("#actionInModuleButton");
    unlockButton("#selectTermsButton");
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
  } else {
    var tmp = [];
    for (item of result.data) {
      tmp.push(item.ref);
    }
    process(tmp);
  }
}

function clearSelectedInfo() {
  $(".status").removeClass("incorrect correct warning").html("");

  $(".selected, .error").removeClass("selected error");
  $(".setRPNButton").css("display", "none");
}

function getParametersURL(componentURL, projectURL, configurationURL) {
  var serverURL = projectURL.split("/process/project-areas/")[0];

  var query = serverURL + "/types?resourceContext=" + projectURL;

  var parameter_req = new XMLHttpRequest();
  parameter_req.open("GET", query, false);
  parameter_req.setRequestHeader("Accept", "application/xml");
  parameter_req.setRequestHeader("Content-Type", "application/xml");
  parameter_req.setRequestHeader("OSLC-Core-Version", "2.0");
  parameter_req.setRequestHeader("Configuration-Context", configurationURL);

  parameter_req.send();
  try {
    var objects = parameter_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rdm/rdf/", "ObjectType");

    for (i = 0; i < objects.length; i++) {
      var object = objects[i];

      for (var c = 0; c < object.childNodes.length; c++) {
        if (object.childNodes[c].nodeName == "dcterms:title") {
          if (object.childNodes[c].innerHTML == parameterName) {
            for (var g = 0; g < object.childNodes.length; g++) {
              if (object.childNodes[g].nodeName == "rm:objectTypeRole") {
                var objTmp = object.childNodes[g];

                if (objTmp.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource")) {
                  var parameterURI = object.getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "about");

                  return parameterURI;
                }
              }
            }
          }
        }
      }
    }
  } catch (err) {
    unlockAllButtons();
    $(".status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");

    return;
  }
}

function getProjectAreaURL(componentURL) {
  var projectArea_req = new XMLHttpRequest();
  projectArea_req.open("GET", componentURL, false);
  projectArea_req.setRequestHeader("Accept", "application/xml");
  projectArea_req.setRequestHeader("Content-Type", "application/xml");

  try {
    projectArea_req.send();

    var project = projectArea_req.responseXML.getElementsByTagNameNS("http://jazz.net/ns/process#", "projectArea");

    var projectURI = project[0].getAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "resource");
    return projectURI;
  } catch (err) {
    unlockAllButtons();
    $(".status").removeClass("warning correct incorrect");
    $(".status").addClass("warning").html(`<b>Error:</b> Parameters were not found`);
    $(".status").addClass("incorrect").html("<b>Error:</b> Something went wrong. Please try again.");
    return;
  }
}

async function getAttributes(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getAttributes(
      ref,
      [
        RM.Data.Attributes.NAME,
        RM.Data.Attributes.PRIMARY_TEXT,
        RM.Data.Attributes.IS_HEADING,
        RM.Data.Attributes.ARTIFACT_TYPE,
        "Pre-Mitigation Severity",
        "Pre-Mitigation Ability to Exploit",
        "Pre-Mitigation Secure Risk Rating",
        "Post-Mitigation Severity",
        "Post-Mitigation Ability to Exploit",
        "Pre-Mitigation Secure Risk Rating",
      ],
      resolve
    );
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function modifyArtifacts(toSave) {
  return new Promise(function (resolve, reject) {
    RM.Data.setAttributes(toSave, resolve);
  });
}

async function createTermLink(source, target) {
  return new Promise(function (resolve, reject) {
    RM.Data.createLink(source, RM.Data.LinkTypes.REFERENCES_TERM, target, resolve);
  });
}

function lockAllButtons() {
  $("#actionButton").attr("disabled", "disabled");
  $("#actionButton").removeClass("btn-primary");
  $("#actionButton").addClass("btn-secondary");
  $("#selectTermsButton").attr("disabled", "disabled");
  $("#selectTermsButton").removeClass("btn-primary");
  $("#selectTermsButton").addClass("btn-secondary");
  $("#findAllTerms").attr("disabled", "disabled");
  $("#findAllTerms").removeClass("btn-primary");
  $("#findAllTerms").addClass("btn-secondary");
  $("#actionInModuleButton").attr("disabled", "disabled");
  $("#actionInModuleButton").removeClass("btn-primary");
  $("#actionInModuleButton").addClass("btn-secondary");
}

function unlockAllButtons() {
  $("#actionButton").removeAttr("disabled");
  $("#actionButton").addClass("btn-primary");
  $("#actionButton").removeClass("btn-secondary");
  $("#selectTermsButton").removeAttr("disabled");
  $("#selectTermsButton").addClass("btn-primary");
  $("#selectTermsButton").removeClass("btn-secondary");
  $("#findAllTerms").removeAttr("disabled");
  $("#findAllTerms").addClass("btn-primary");
  $("#findAllTerms").removeClass("btn-secondary");

  if (moduleRef == null) return;
  $("#actionInModuleButton").removeAttr("disabled");
  $("#actionInModuleButton").addClass("btn-primary");
  $("#actionInModuleButton").removeClass("btn-secondary");
}

function lockButton(button) {
  $(button).attr("disabled", "disabled");
  $(button).removeClass("btn-primary");
  $(button).addClass("btn-secondary");
}

function unlockButton(button) {
  if (button == "#actionInModuleButton" && moduleRef == null) return;
  $(button).removeAttr("disabled");
  $(button).addClass("btn-primary");
  $(button).removeClass("btn-secondary");
}

async function getModuleArtifacts(ref) {
  return new Promise(function (resolve, reject) {
    RM.Data.getContentsAttributes(ref, [RM.Data.Attributes.PRIMARY_TEXT], resolve);
  });
}

function uriToPublishURI(uri) {
  var tmp = uri.replace("/resources/", "/publish/resources?resourceURI=");
  var url_req = new XMLHttpRequest();
  url_req.open("GET", tmp, false);
  url_req.setRequestHeader("Accept", "application/xml");
  url_req.setRequestHeader("Content-Type", "application/xml");

  try {
    url_req.send();
    var url = url_req.responseXML.getElementsByTagNameNS("http://www.ibm.com/xmlns/rrm/1.0/", "core");
    var newURL = url[0].innerHTML;

    if (newURL) return uri.split("/resources/")[0] + "/resources/" + newURL;
    else return uri;
  } catch (err) {
    return;
  }
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}
